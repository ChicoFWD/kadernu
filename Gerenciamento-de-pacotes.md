# Gerenciamento de pacotes

## Gerenciador de pacotes para Windows

<h2>

![Registro online de pacotes do Window](https://chocolatey.org/assets/images/community-mockup.gif)

[Instalando Chocolatey](https://chocolatey.org/install)

[Configurar e instalar com Powershell](https://docs.chocolatey.org/en-us/choco/setup#install-with-powershell.exe)

[Políticas de execução do PowerShell](https://docs.microsoft.com/pt-br/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2)

</h2>

