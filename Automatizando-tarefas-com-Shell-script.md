# Automatizando tarefas com Shell script

Um script sinmples para instalar a ferramanda de provisionamento Terraform

```sh
# Atribui permissão de execução ao script com o comando
chmod a+x /terraform-installer.sh

# Executa o script
./terraform-installer.sh
```
