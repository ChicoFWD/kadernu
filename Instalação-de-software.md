# Instalação de software

## Instalações de software Windows

<h2>

[Trabalhando com instalações de software](https://docs.microsoft.com/pt-br/powershell/scripting/samples/working-with-software-installations?view=powershell-7.2)

[Usar um script para instalar um aplicativo de área de trabalho em pacotes de provisionamento](https://docs.microsoft.com/pt-br/windows/configuration/provisioning-packages/provisioning-script-to-install-app)

</h2>

### Pacotes de provisionamento

<h3>

[Código de Conduta de Código Aberto da Microsoft](https://github.com/MicrosoftDocs/windows-itpro-docs/tree/public/windows/configuration/provisioning-packages)

</h3>
