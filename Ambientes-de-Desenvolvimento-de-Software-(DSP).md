# Ambientes de Desenvolvimento de Software (DPS)

##

[Configurar seu ambiente de desenvolvimento no Windows](https://docs.microsoft.com/pt-br/windows/dev-environment/)

[Configurar um ambiente de desenvolvimento WSL](https://docs.microsoft.com/pt-br/windows/wsl/setup/environment)

##

###

<h3>

[Conceito: Ambiente de Desenvolvimento](https://www.cin.ufpe.br/~gta/rup-vc/core.base_rup/guidances/concepts/development_environment_84567088.html)
[Construção de um Ambiente de Desenvolvimento de Software baseado em um Sistema de Gerencia de Workflow e outros Produtos Comarciais - Dissertação por Carlos Michel Betemps](https://www.lume.ufrgs.br/bitstream/handle/10183/3404/000387523.pdf)

</h3>
