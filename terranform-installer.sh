#!/usr/bin/env bash

# Depends on:
# unzip

# Terraform Documentation: https://www.terraform.io/docs

export TERRAFORM_VERSION="1.1.6" # latest, stable version

wget "https://releases.hashicorp.com/terraform/"$TERRAFORM_VERSION"/terraform_"$TERRAFORM_VERSION"_linux_amd64.zip"

printenv | grep TERRAFORM_VERSION

unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip

sudo mv terraform /usr/local/bin/ 

sudo chown root:root /usr/local/bin/terraform

terraform -v
